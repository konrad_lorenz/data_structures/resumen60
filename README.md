# Resumen de Conceptos de Ciencia de Datos y Algoritmos

## Grafos Ponderados
En un **grafo ponderado**, cada arista tiene un valor asociado, conocido como peso. Ejemplo: en un mapa, las ciudades pueden ser nodos y las carreteras aristas, con pesos representando la distancia.

## Grafos No Ponderados
Un **grafo no ponderado** no tiene pesos en sus aristas. Ejemplo: en una red social, las personas pueden ser nodos y las amistades aristas, sin pesos asociados.

## Grafos No Dirigidos
Un **grafo no dirigido** permite que el movimiento a través de sus aristas sea bidireccional. Ejemplo: en un mapa de rutas de senderismo, puedes ir de un punto a otro y viceversa.

## Grafos Dirigidos
En un **grafo dirigido**, las aristas tienen una dirección definida. Ejemplo: en un sistema de navegación, una calle de sentido único sería una arista dirigida.

## Árboles Binarios
Un **árbol binario** es una estructura de datos en la que cada nodo tiene como máximo dos hijos, conocidos como hijo izquierdo y derecho. Ejemplo: en la organización de archivos y carpetas, cada carpeta (nodo) puede contener hasta dos subcarpetas.

## Árboles B
Un **árbol B** es una estructura de datos de árbol balanceado, ideal para sistemas que leen y escriben grandes bloques de datos. Son comúnmente utilizados en bases de datos.

## Búsqueda en Anchura (BFS)
La **Búsqueda en Anchura** explora los vecinos de un nodo antes de moverse a los siguientes niveles. Ejemplo: en un laberinto, BFS exploraría todas las rutas cercanas antes de adentrarse más.

## Búsqueda en Profundidad (DFS)
La **Búsqueda en Profundidad** explora un camino completamente antes de retroceder y probar otra ruta. Ejemplo: en un árbol genealógico, DFS seguiría una línea de descendencia hasta el final antes de pasar a otra.

## Búsqueda en Grafos Ponderados
La **búsqueda en grafos ponderados** implica encontrar la ruta más corta o más eficiente considerando los pesos de las aristas. El algoritmo de Dijkstra es un ejemplo.

## Algoritmo de Floyd
El **Algoritmo de Floyd** es un algoritmo de caminos mínimos que encuentra la ruta más corta entre todos los pares de nodos en un grafo ponderado.

## Índices Invertidos
Los **índices invertidos** se utilizan en motores de búsqueda para almacenar qué palabras clave aparecen en qué documentos, facilitando búsquedas rápidas.

## Caches
Un **cache** es un almacenamiento de acceso rápido que guarda copias de datos frecuentemente utilizados para mejorar los tiempos de acceso.

## Colas con SQS
**SQS (Simple Queue Service)** de AWS es un servicio de colas que permite el desacoplamiento y escalabilidad entre componentes de un sistema. Las colas gestionan y ordenan las tareas a procesar.

## Estructuras de Datos Distribuidas
Las **estructuras de datos distribuidas** son aquellas que se extienden y operan en múltiples nodos en una red, como en el caso de bases de datos distribuidas.

## Tensores en Estructuras de Datos
Un **tensor** es una generalización de vectores y matrices a dimensiones superiores y se utiliza ampliamente en el aprendizaje profundo y análisis de datos multidimensionales.

## Algoritmos de Búsqueda Más Usados
- **Búsqueda Binaria**: eficiente en listas ordenadas.
- **BFS y DFS**: para grafos y árboles.
- **Búsqueda de Hash**: rápida para búsquedas de coincidencia directa.

## Algoritmos de Ordenamiento Más Usados
- **Quicksort**: eficiente en promedio, con particiones.
- **Mergesort**: basado en división y conquista.
- **Bubblesort**: simple pero menos eficiente.

## Ordenamiento de Árboles (Inorder, Preorder, Postorder)
- **Inorder**: Izquierda, Raíz, Derecha.
- **Preorder**: Raíz, Izquierda, Derecha.
- **Postorder**: Izquierda, Derecha, Raíz.

## Grafos con Neo4j
**Neo4j** es una base de datos de grafos que permite almacenar y consultar relaciones entre datos de manera eficiente.

## Concurrencia
La **concurrencia** se refiere a la gestión de múltiples tareas al mismo tiempo en un sistema.

## Paralelismo
El **paralelismo** implica la ejecución simultánea de tareas en múltiples procesadores.

## Sistemas Distribuidos
Los **sistemas distribuidos** son colecciones de computadoras independientes que aparecen ante el usuario como un solo sistema.


# Listado de Referencias y Herramientas

## Grafos Ponderados
- **Referencia**: "Introduction to Algorithms" de Thomas H. Cormen
- **Herramienta**: [NetworkX](https://networkx.org/) (Python)

## Grafos No Ponderados
- **Referencia**: "Graph Theory" de Reinhard Diestel
- **Herramienta**: [Graphviz](https://www.graphviz.org/)

## Grafos No Dirigidos
- **Referencia**: "Graph Theory with Applications" de Bondy y Murty
- **Herramienta**: [Gephi](https://gephi.org/)

## Grafos Dirigidos
- **Referencia**: "Directed Graphs" en "Discrete Mathematics and its Applications" de Kenneth Rosen
- **Herramienta**: [Cytoscape](https://cytoscape.org/)

## Árboles Binarios
- **Referencia**: "Data Structures and Algorithms in Java" de Robert Lafore
- **Herramienta**: [Binary Tree Visualizer](https://www.cs.usfca.edu/~galles/visualization/BST.html)

## Árboles B
- **Referencia**: "Database System Concepts" de Silberschatz, Korth, Sudarshan
- **Herramienta**: [B-Tree Visualization](https://www.cs.usfca.edu/~galles/visualization/BTree.html)

## Búsqueda en Anchura (BFS)
- **Referencia**: "Algorithms" de Robert Sedgewick y Kevin Wayne
- **Herramienta**: [VisuAlgo](https://visualgo.net/en/dfsbfs)

## Búsqueda en Profundidad (DFS)
- **Referencia**: "Algorithms" de Robert Sedgewick y Kevin Wayne
- **Herramienta**: [VisuAlgo](https://visualgo.net/en/dfsbfs)

## Búsqueda en Grafos Ponderados
- **Referencia**: "Introduction to Algorithms" de Cormen, Leiserson, Rivest, y Stein
- **Herramienta**: [Dijkstra Algorithm Tool](https://www.cs.usfca.edu/~galles/visualization/Dijkstra.html)

## Algoritmo de Floyd
- **Referencia**: "Introduction to Algorithms" de Cormen, Leiserson, Rivest, y Stein
- **Herramienta**: [Python Floyd Algorithm Implementation](https://www.geeksforgeeks.org/floyd-warshall-algorithm-dp-16/)

## Índices Invertidos
- **Referencia**: "Information Retrieval" de Christopher D. Manning, Prabhakar Raghavan y Hinrich Schütze
- **Herramienta**: [Apache Lucene](https://lucene.apache.org/)

## Caches
- **Referencia**: "High Performance Browser Networking" de Ilya Grigorik
- **Herramienta**: [Redis](https://redis.io/)

## Colas con SQS
- **Referencia**: "Amazon Simple Queue Service Developer Guide"
- **Herramienta**: [Amazon SQS](https://aws.amazon.com/sqs/)

## Estructuras de Datos Distribuidas
- **Referencia**: "Designing Data-Intensive Applications" de Martin Kleppmann
- **Herramienta**: [Apache Cassandra](https://cassandra.apache.org/_/index.html)

## Tensores en Estructuras de Datos
- **Referencia**: "Deep Learning" de Ian Goodfellow, Yoshua Bengio y Aaron Courville
- **Herramienta**: [TensorFlow](https://www.tensorflow.org/)

## Algoritmos de Búsqueda Más Usados
- **Referencia**: "Algorithms" de Robert Sedgewick y Kevin Wayne
- **Herramienta**: [Elasticsearch](https://www.elastic.co/elasticsearch/)

## Algoritmos de Ordenamiento Más Usados
- **Referencia**: "Algorithms" de Robert Sedgewick y Kevin Wayne
- **Herramienta**: [Python Sort Algorithms](https://www.python.org/)

## Ordenamiento de Árboles (Inorder, Preorder, Postorder)
- **Referencia**: "Data Structures and Algorithms in Java" de Robert Lafore
- **Herramienta**: [Tree Traversal Visualizer](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)

## Grafos con Neo4j
- **Referencia**: "Graph Databases" de Ian Robinson, Jim Webber, y Emil Eifrem
- **Herramienta**: [Neo4j](https://neo4j.com/)

## Concurrencia
- **Referencia**: "Concurrency in Practice" de Brian Goetz
- **Herramienta**: [Java Concurrency Utilities](https://docs.oracle.com/javase/8/docs/technotes/guides/concurrency/)

## Paralelismo
- **Referencia**: "Parallel Programming in C with MPI and OpenMP" de Michael J. Quinn
- **Herramienta**: [OpenMP](https://www.openmp.org/)

## Sistemas Distribuidos
- **Referencia**: "Distributed Systems: Principles and Paradigms" de Andrew S. Tanenbaum y Maarten Van Steen
- **Herramienta**: [Apache Hadoop](https://hadoop.apache.org/)
